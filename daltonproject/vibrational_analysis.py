from typing import NamedTuple, Optional

import numpy as np
from qcelemental import PhysicalConstantsContext, periodictable
from spectroscpy import get_raman_intensities, get_vib_harm_freqs_and_eigvecs

from .molecule import Molecule
from .program import PolarizabilityGradients

__all__ = ('VibrationalAnalysis', 'vibrational_analysis', 'compute_ir_intensities', 'compute_raman_intensities')

constants = PhysicalConstantsContext('CODATA2018')


class VibrationalAnalysis(NamedTuple):
    frequencies: np.ndarray
    ir_intensities: Optional[np.ndarray] = None
    raman_intensities: Optional[np.ndarray] = None
    # TODO check exactly which one it is
    cartesian_displacements: Optional[np.ndarray] = None


def vibrational_analysis(molecule: Molecule,
                         hessian: np.ndarray,
                         dipole_gradients: Optional[np.ndarray] = None,
                         polarizability_gradients: Optional[PolarizabilityGradients] = None
                         ) -> VibrationalAnalysis:
    """Perform vibrational analysis.

    Args:
        molecule: Molecule object.
        hessian: Second derivatives with respect to nuclear displacements in
            Cartesian coordinates.
        dipole_gradients: Gradient of the dipole moment with respect to nuclear
            displacements in Cartesian coordinates.
        polarizability_gradients: Gradient of the polarizability with respect to
            nuclear displacements in Cartesian coordinates.

    Returns:
        Harmonic vibrational frequencies (cm^-1). Optionally, IR intensities
        (m^2 / (s * mol)), Raman intensities (C^4 * s^2 / (J * m^2 * kg)), and
        transformation matrix.
    """
    coordinates = molecule.coordinates / constants.bohr2angstroms
    atomic_numbers = np.array([periodictable.to_atomic_number(element) for element in molecule.elements])
    energies, transformation_matrix, _ = get_vib_harm_freqs_and_eigvecs(coords=coordinates,
                                                                        charges=atomic_numbers,
                                                                        masses=molecule.masses,
                                                                        hess=hessian,
                                                                        outproj=True,
                                                                        print_level=0)
    vibrational_frequencies = energies * constants.hartree2wavenumbers
    ir_intensities = None
    if dipole_gradients is not None:
        ir_intensities = compute_ir_intensities(dipole_gradients, transformation_matrix)
    raman_intensities = None
    if polarizability_gradients is not None:
        raman_intensities = []
        for gradients, frequency in zip(polarizability_gradients.values, polarizability_gradients.frequencies):
            raman_intensities.append(
                compute_raman_intensities(gradients, frequency, vibrational_frequencies, transformation_matrix))
        raman_intensities = np.array(raman_intensities)
    return VibrationalAnalysis(frequencies=vibrational_frequencies,
                               ir_intensities=ir_intensities,
                               raman_intensities=raman_intensities,
                               cartesian_displacements=transformation_matrix)


def compute_ir_intensities(dipole_gradients: np.ndarray,
                           transformation_matrix: np.ndarray,
                           is_linear: bool = False) -> np.ndarray:
    """Compute infrared molar decadic absorption coefficient in m^2 / (s * mol).

    Args:
        dipole_gradients: Gradients of the dipole moment (au) with respect to
            nuclear displacements in Cartesian coordinates.
        transformation_matrix: Transformation matrix to convert from Cartesian
            to normal coordinates.
        is_linear: Indicate if molecule is linear.

    Returns:
        IR intensities in m^2 / (s * mol).
    """
    normal_dipole_gradients = transformation_matrix.T @ dipole_gradients
    if is_linear:
        normal_dipole_gradients = normal_dipole_gradients[:-5]
    else:
        normal_dipole_gradients = normal_dipole_gradients[:-6]
    ir_intensities = []
    normal_dipole_gradients_si = normal_dipole_gradients * constants.get('elementary charge') / np.sqrt(constants.me)
    prefactor = constants.na / (12.0 * np.log(10) * constants.e0 * constants.c)
    for dipole_gradient in normal_dipole_gradients_si:
        ir_intensities.append(prefactor * np.sum(dipole_gradient**2))
    return np.array(ir_intensities)


def compute_raman_intensities(polarizability_gradients: np.ndarray,
                              polarizability_frequency: float,
                              vibrational_frequencies: np.ndarray,
                              transformation_matrix: np.ndarray,
                              is_linear: bool = False) -> np.ndarray:
    """Compute Raman differential scattering cross-section in C^4 * s^2 / (J * m^2 * kg).

    Args:
        polarizability_gradients: Gradients of the polarizability with respect
            to nuclear displacements in Cartesian coordinates.
        polarizability_frequency: Frequency of the incident light in au.
        vibrational_frequencies: Vibrational frequencies in cm^-1.
        transformation_matrix: Transformation matrix to convert from Cartesian
            to normal coordinates.
        is_linear: Indicate if molecule is linear.

    Returns:
        Raman intensities in C^4 * s^2 / (J * m^2 * kg).
    """
    normal_polar_gradient = np.zeros(polarizability_gradients.shape)
    for i in range(3):
        normal_polar_gradient[:, :, i] = transformation_matrix.T @ polarizability_gradients[:, :, i]
    if is_linear:
        normal_polar_gradient = normal_polar_gradient[:-5, :, :]
    else:
        normal_polar_gradient = normal_polar_gradient[:-6, :, :]
    vibrational_energies = vibrational_frequencies / constants.hartree2wavenumbers
    raman_intensities = get_raman_intensities(specifications=['Raman: SCS 45+7, SI arb units'],
                                              au_incident_vibration_energy=polarizability_frequency,
                                              au_polarizability_gradients=normal_polar_gradient,
                                              au_vibrational_energies=vibrational_energies,
                                              print_level=0,
                                              temperature=298.15)
    return np.array(raman_intensities)
