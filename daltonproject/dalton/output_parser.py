"""Parse Dalton output files"""
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

import re
import tarfile
from typing import Dict, List, Union

import numpy as np
from qcelemental import PhysicalConstantsContext

from .. import program
from ..program import (HyperfineCouplings, NumBasisFunctions, NumOrbitals, OrbitalEnergies, OrbitalEnergy,
                       PolarizabilityGradients)

constants = PhysicalConstantsContext('CODATA2018')


class OutputParser(program.OutputParser):
    """Parse Dalton output files."""

    def __init__(self, filename: str) -> None:
        self._filename = filename

    @property
    def filename(self) -> str:
        """Name of the Dalton output files without the extension."""
        return self._filename

    @property
    def energy(self) -> float:
        """Extract the final energy from Dalton output file."""
        scf = r'\n@.*Final.*energy: +(.*)\n'
        mp2 = r'\n@.*second.*order.*energy.*: +(.*)\n'
        cc = r'\n.*Total.*CC.*energy: +(.*)\n'
        with open(f'{self.filename}.out', 'r') as output_file:
            matches = re.findall(pattern=f'{scf}|{mp2}|{cc}', string=output_file.read())
        if not matches:
            raise Exception(f'Energy not found in {self.filename}.')
        scf_energies = [match[0] for match in matches]
        mp2_energies = [match[1] for match in matches]
        cc_energies = [match[2] for match in matches]
        if any(cc_energies):
            energy = float([energy for energy in cc_energies if energy][-1])
        elif any(mp2_energies):
            energy = float([energy for energy in mp2_energies if energy][-1])
        elif any(scf_energies):
            energy = float([energy for energy in scf_energies if energy][-1])
        else:
            raise Exception(f'Neither SCF, MP2, nor CC energy found in {self.filename}.')
        return energy

    @property
    def dipole(self) -> np.ndarray:
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'Dipole moment components' in line:
                    break
            output_file.readline()  # ------------------------
            output_file.readline()  # blank line
            output_file.readline()  # au               Debye          C m (/(10**-30)
            output_file.readline()  # blank line
            x = output_file.readline().split()[1]
            y = output_file.readline().split()[1]
            z = output_file.readline().split()[1]
        return np.array([x, y, z]).astype(np.float64)

    @property
    def polarizability(self) -> np.ndarray:
        polarizability = np.zeros((3, 3))
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if '@ FREQUENCY INDEPENDENT SECOND ORDER PROPERTIES' in line:
                    break
            output_file.readline()
            # @ -<< XDIPLEN  ; YDIPLEN  >> =  2.417027806004D+01
            # @ -<< XDIPLEN  ; YDIPLEN  >> = -2.558653214742D+00
            # @ -<< XDIPLEN  ; ZDIPLEN  >> = -5.754013493496D-01
            # @ -<< YDIPLEN  ; YDIPLEN  >> =  2.665939251496D+01
            # @ -<< YDIPLEN  ; ZDIPLEN  >> = -7.790305489343D-01
            # @ -<< ZDIPLEN  ; ZDIPLEN  >> =  2.240505334596D+01
            polarizability[0, 0] = float(output_file.readline().split()[-1].replace('D', 'E'))
            polarizability[0, 1] = float(output_file.readline().split()[-1].replace('D', 'E'))
            polarizability[0, 2] = float(output_file.readline().split()[-1].replace('D', 'E'))
            polarizability[1, 1] = float(output_file.readline().split()[-1].replace('D', 'E'))
            polarizability[1, 2] = float(output_file.readline().split()[-1].replace('D', 'E'))
            polarizability[2, 2] = float(output_file.readline().split()[-1].replace('D', 'E'))
            polarizability[1, 0] = polarizability[0, 1]
            polarizability[2, 0] = polarizability[0, 2]
            polarizability[2, 1] = polarizability[1, 2]
        return polarizability

    @property
    def first_hyperpolarizability(self) -> np.ndarray:
        beta = np.zeros((3, 3, 3))
        lab2idx = {'X': 0, 'Y': 1, 'Z': 2}
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'Results from quadratic response calculation' in line:
                    break
            output_file.readline()
            output_file.readline()
            output_file.readline()
            output_file.readline()
            line = output_file.readline()
            while 'B-freq' in line:
                split = line.split()
                components = split[7][5], split[7][7], split[7][9]
                lhs_indices = tuple([lab2idx[component] for component in components])
                value = split[-1]
                if 'beta' in value:
                    components = value[5], value[7], value[9]
                    rhs_indices = tuple([lab2idx[component] for component in components])
                    beta[lhs_indices] = beta[rhs_indices]
                else:
                    beta[lhs_indices] = float(value)
                line = output_file.readline()
        return beta

    @property
    def electronic_energy(self) -> float:
        """Extract the electronic energy from Dalton output file."""
        return self.energy - self.nuclear_repulsion_energy

    @property
    def nuclear_repulsion_energy(self) -> float:
        """Extract the nuclear repulsion energy from Dalton output file."""
        with open(f'{self.filename}.out', 'r') as output_file:
            matches = re.findall(pattern=r'\n@.*Nuclear repulsion: +(.*)\n', string=output_file.read())
        if matches == []:
            raise Exception(f'Nuclear repulsion energy not found in {self.filename}.')
        energies = [match[0] for match in matches]
        energy = float([energy for energy in energies if energy][-1])
        return energy

    @property
    def num_electrons(self) -> int:
        """Extract number of electrons from Dalton output file."""
        with open(f'{self.filename}.out', 'r') as output_file:
            match = re.search(pattern=r'\n.*Number of electrons : +(.*)\n', string=output_file.read())
        if match is None:
            raise Exception(f'Number of electrons not found in {self.filename}.')
        return int(match.group(1))

    @property
    def num_orbitals(self) -> NumOrbitals:
        """Extract number of orbitals from Dalton output file.

        This number can be smaller than the number of basis functions, in case of linear dependencies.
        """
        found_num_orbitals = False
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'Total number of orbitals' in line:
                    num_orbitals_per_sym = [int(i) for i in line.split('|')[1].split()]
                    found_num_orbitals = True
                    break
        if not found_num_orbitals:
            raise Exception(f'Number of orbitals not found in {self.filename}.')
        num_orbitals = sum(num_orbitals_per_sym)
        return NumOrbitals(tot_num_orbitals=num_orbitals, num_orbitals_per_sym=num_orbitals_per_sym)

    @property
    def num_basis_functions(self) -> NumBasisFunctions:
        """Extract number of basis functions from Dalton output file."""
        found_num_basis_functions = False
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'Number of basis functions' in line:
                    num_basis_functions_per_sym = [int(i) for i in line.split('|')[1].split()]
                    found_num_basis_functions = True
                    break
        if not found_num_basis_functions:
            raise Exception(f'Number of basis functions not found in {self.filename}.')
        num_basis_functions = sum(num_basis_functions_per_sym)
        return NumBasisFunctions(tot_num_basis_functions=num_basis_functions,
                                 num_basis_functions_per_sym=num_basis_functions_per_sym)

    @property
    def mo_energies(self) -> OrbitalEnergies:
        """Extract molecular orbital energies from Dalton output file."""
        symmetry_species = extract_symmetry_species(self.filename)
        mo_energies: Dict[str, List[float]] = {sym_name: [] for sym_name in symmetry_species}
        with open(f'{self.filename}.out', 'r') as f:
            lines = f.readlines()
        found_energies = False
        for line in lines:
            if line == '\n':
                continue
            elif 'E(LUMO)' in line:
                break
            elif 'Sym       Hartree-Fock orbital energies' in line:
                found_energies = True
                continue
            elif 'Sym       Kohn-Sham orbital energies' in line:
                found_energies = True
                continue
            elif found_energies and line[0:6] != '      ':
                current_symmetry = line.split()[1]
            if found_energies:
                for energy in line[6:].split():
                    # TODO fix "might be referenced before assignment"
                    mo_energies[current_symmetry].append(float(energy))
        if not found_energies:
            raise Exception(f'Could not find molecular orbital energies in {self.filename}.')
        mo_energies_per_sym = {}
        for key, value in mo_energies.items():
            mo_energies_per_sym[key] = np.array(value)
        all_orbital_energies: List[float] = []
        for mo_energies_sym in mo_energies_per_sym.values():
            all_orbital_energies += mo_energies_sym.tolist()
        return OrbitalEnergies(orbital_energies=np.array(sorted(all_orbital_energies)),
                               orbital_energies_per_sym=mo_energies_per_sym)

    @property
    def homo_energy(self) -> OrbitalEnergy:
        """Extract the highest occupied molecular orbital energy from the Dalton output file."""
        with open(f'{self.filename}.out', 'r') as f:
            lines = [line.strip() for line in f.readlines()]
        symmetry_species = extract_symmetry_species(self.filename)
        for line in lines:
            if 'E(HOMO)' in line:
                energy = float(line.split()[3])
                symmetry = symmetry_species[int(line.split()[6].strip(')')) - 1]
                break
        else:
            raise Exception(f'Could not find HOMO energy in {self.filename}.')
        return OrbitalEnergy(orbital_energy=energy, symmetry=symmetry)

    @property
    def lumo_energy(self) -> OrbitalEnergy:
        """Extract the lowest unoccupied molecular orbital energy from the Dalton output file."""
        symmetry_species = extract_symmetry_species(self.filename)
        with open(f'{self.filename}.out', 'r') as f:
            lines = [line.strip() for line in f.readlines()]
        for line in lines:
            if 'E(LUMO)' in line:
                energy = float(line.split()[2])
                symmetry = symmetry_species[int(line.split()[5].strip(')')) - 1]
                break
        else:
            raise Exception(f'Could not find LUMO energy in {self.filename}.')
        return OrbitalEnergy(orbital_energy=energy, symmetry=symmetry)

    @property
    def excitation_energies(self) -> np.ndarray:
        """Extract one-photon excitation energies from Dalton output file."""
        # TODO handle symmetry (use ExcitationEnergies)
        excitation_energies: List[float] = []
        response = False
        excita = False
        tpa = False
        ccrsp = False
        # find out which type of excitation energy calculation
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'Content of the .dal input file' in line:
                    line = output_file.readline()
                    while 'Content of the .mol file' not in line:
                        if 'RESPONSE' in line:
                            response = True
                        if 'EXCITA' in line:
                            excita = True
                        if 'TWO-PHOTON' in line:
                            tpa = True
                        if 'CCEXCI' in line:
                            ccrsp = True
                        line = output_file.readline()
        if excita:
            with open(f'{self.filename}.out', 'r') as output_file:
                for line in output_file:
                    if 'Sym.   Mode        Frequency     Oscillator-strength' in line:
                        output_file.readline()
                        output_file.readline()
                        excitation = output_file.readline().split()
                        while '@' in excitation:
                            excitation_energies.append(float(excitation[3]))
                            excitation = output_file.readline().split()
                    if 'Triplet electronic excitation energies' in line:
                        output_file.readline()
                        output_file.readline()
                        output_file.readline()
                        output_file.readline()
                        output_file.readline()
                        output_file.readline()
                        excitation = output_file.readline().split()
                        while len(excitation) == 4:
                            excitation_energies.append(float(excitation[3]))
                            excitation = output_file.readline().split()
        elif response and tpa:
            with open(f'{self.filename}.out', 'r') as output_file:
                # Read excitation energies from start of where "RSPCTL MICROITERATIONS" have converged,
                # until all have been printed (at the specification of the quadratic response calculations).
                for line in output_file:
                    if '*** RSPCTL MICROITERATIONS CONVERGED' in line:
                        break
                for line in output_file:
                    if '@ Singlet excitation energy' in line:
                        excitation_energy = float(line.split()[-1].replace('D', 'E')) * constants.hartree2ev
                        excitation_energies.append(excitation_energy)
                    elif 'Linear response calculations for quadratic response' in line:
                        break
        elif response and not tpa:
            with open(f'{self.filename}.out', 'r') as output_file:
                # Read excitation energies from start of where "RSPCTL MICROITERATIONS" have converged,
                for line in output_file:
                    if '@ Excitation energy' in line:
                        excitation_energy = float(line.split()[-2].replace('D', 'E')) * constants.hartree2ev
                        excitation_energies.append(excitation_energy)
        elif ccrsp:
            with open(f'{self.filename}.out', 'r') as output_file:
                for line in output_file:
                    if 'RESULTS FOR ONE-PHOTON ABSORPTION STRENGTHS !' in line:
                        while 'SUMMARY OF COUPLED CLUSTER CALCULATION' not in line:
                            if 'frequency' in line:
                                excitation_energies.append(float(line.split()[4]))
                            line = output_file.readline()
        else:
            # TODO add message to exception
            raise NotImplementedError
        if not excitation_energies:
            raise Exception(f'Could not find excitation energies in {self.filename}.')
        return np.array(excitation_energies).astype(np.float64)

    @property
    def oscillator_strengths(self) -> np.ndarray:
        """Extract one-photon oscillator strengths from Dalton output file."""
        # TODO handle symmetry
        response = False
        excita = False
        ccrsp = False
        # find out which type of excitation energy calculation
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'Content of the .dal input file' in line:
                    line = output_file.readline()
                    while 'Content of the .mol file' not in line:
                        if 'RESPONSE' in line:
                            response = True
                        if 'EXCITA' in line:
                            excita = True
                        if 'TWO-PHOTON' in line:
                            raise ValueError('One-photon oscillator strengths not available from TPA calculation.')
                        if 'CCEXCI' in line:
                            ccrsp = True
                        line = output_file.readline()
        oscillator_strengths = []
        if excita:
            with open(f'{self.filename}.out', 'r') as output_file:
                for line in output_file:
                    if 'Sym.   Mode        Frequency     Oscillator-strength' in line:
                        output_file.readline()
                        output_file.readline()
                        excitation = output_file.readline().split()
                        while '@' in excitation:
                            oscillator_strengths.append(excitation[5])
                            excitation = output_file.readline().split()
        if ccrsp:
            with open(f'{self.filename}.out', 'r') as output_file:
                for line in output_file:
                    if 'RESULTS FOR ONE-PHOTON ABSORPTION STRENGTHS !' in line:
                        while 'SUMMARY OF COUPLED CLUSTER CALCULATION' not in line:
                            if 'oscillator strength' in line:
                                oscillator_strengths.append(line.split()[5])
                            line = output_file.readline()
        elif response:
            oscillator_strengths_directional = []
            with open(f'{self.filename}.out', 'r') as output_file:
                # the oscillator_strengths are split into components by direction x, y, z
                # collect all, reshape and sum over x, y, z
                for line in output_file:
                    if '@ Oscillator strength (LENGTH) ' in line:
                        oscillator_strength = float(line.split()[5].replace('D', 'E'))
                        oscillator_strengths_directional.append(oscillator_strength)
            oscillator_strengths_directional = np.array(oscillator_strengths_directional).reshape(-1, 3)
            oscillator_strengths = list(np.sum(oscillator_strengths_directional, axis=1))
        if not oscillator_strengths:
            raise Exception(f'Could not find oscillator strengths in {self.filename}.')
        return np.array(oscillator_strengths).astype(np.float64)

    @property
    def two_photon_tensors(self) -> np.ndarray:
        """Extract two-photon transition tensors from Dalton output file."""
        # TODO handle symmetry
        two_photon_tensors = []
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'Sym  No  Energy        Sxx        Syy        Szz        Sxy        Sxz        Syz' in line:
                    output_file.readline()
                    break
            else:
                raise Exception(f'Could not find two-photon matrix elements in {self.filename}.')
            for line in output_file:
                if '----' in line:
                    break
                # using line.split() is no good, as the output may or may not have spaces between the elements
                xx = float(line[20:28].strip())
                yy = float(line[28:36].strip())
                zz = float(line[36:44].strip())
                xy = float(line[44:52].strip())
                xz = float(line[52:60].strip())
                yz = float(line[60:68].strip())
                elements = [[xx, xy, xz], [xy, yy, yz], [xz, yz, zz]]
                two_photon_tensors.append(elements)
        return np.array(two_photon_tensors).astype(np.float64)

    @property
    def two_photon_strengths(self) -> np.ndarray:
        """Extract two-photon transition strengths from Dalton output file."""
        # TODO handle symmetry
        two_photon_strengths = []
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'Sym  No  Energy  Polarization         Df         Dg          D      sigma       R' in line:
                    output_file.readline()
                    excitation = output_file.readline().split()
                    while len(excitation) > 1:
                        if 'Circular' in excitation:
                            excitation = output_file.readline().split()
                            continue
                        two_photon_strengths.append(excitation[6])
                        excitation = output_file.readline().split()
        if not two_photon_strengths:
            raise Exception(f'Could not find two-photon strengths in {self.filename}.')
        return np.array(two_photon_strengths).astype(np.float64)

    @property
    def two_photon_cross_sections(self) -> np.ndarray:
        """Extract two-photon cross-sections from Dalton output file."""
        # TODO handle symmetry
        two_photon_strengths = []
        with open(f'{self.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'Sym  No  Energy  Polarization         Df         Dg          D      sigma       R' in line:
                    output_file.readline()
                    excitation = output_file.readline().split()
                    while len(excitation) > 1:
                        if 'Circular' in excitation:
                            excitation = output_file.readline().split()
                            continue
                        two_photon_strengths.append(excitation[7])
                        excitation = output_file.readline().split()
        if not two_photon_strengths:
            raise Exception(f'Could not find two-photon cross-sections in {self.filename}.')
        return np.array(two_photon_strengths).astype(np.float64)

    @property
    def orbital_coefficients(self) -> Union[Dict[int, np.ndarray], np.ndarray]:
        """Extract orbital coefficients from Dalton output archive (.tar.gz).

        This assumes that .PUNCHOUTORBITALS is used in the Dalton input file.
        """
        # TODO use NamedTuple?
        with tarfile.open(f'{self.filename}.tar.gz', 'r:gz') as tar_file:
            names = tar_file.getnames()
            if 'DALTON.MOPUN' not in names:
                raise Exception(f'Could not find orbital file (DALTON.MOPUN) in {self.filename}.tar.gz.')
            with tar_file.extractfile(tar_file.getmember('DALTON.MOPUN')) as mopun_file:  # type: ignore
                if mopun_file is None:
                    raise Exception(f'Orbital file (DALTON.MOPUN) in {self.filename}.tar.gz is empty.')
                mopun_content = mopun_file.read().decode('utf-8')
        num_bf = self.num_basis_functions[1]
        mo_coefficients = {}
        for i, orbitals in enumerate(num_bf, 1):
            mo_coefficients[i] = np.zeros(orbitals * orbitals)
        idx = 0
        current_symmetry = 1
        for line in mopun_content.split('\n'):
            if '**MOLORB' in line:
                continue
            if line == '':
                continue
            # Find the next symmetry with more than zero basis functions.
            for i in range(current_symmetry, len(num_bf) + 1):
                if num_bf[current_symmetry - 1] != 0:
                    break
                current_symmetry += 1
            # MOPUN is fixed format, therefore the numbers can be found by taking steps of 18.
            for i in range(0, len(line) - 17, 18):
                mo_coefficients[current_symmetry][idx] = float(line[i:i + 18])
                idx += 1
            # If number of basis functions in a symmetry have been found number of basis functions times,
            # then it is time to move to next symmetry.
            if num_bf[current_symmetry - 1]**2 == idx:
                idx = 0
                current_symmetry += 1
        for i, orbitals in enumerate(num_bf, 1):
            # MOPUN is ordered as basis_functions x orbitals, often orbitals x basis_functions is expected.
            mo_coefficients[i] = mo_coefficients[i].reshape((orbitals, orbitals)).T
        if len(num_bf) == 1:
            # If only one symmetry, return an array instead of a dict.
            mo_coefficients = mo_coefficients[1]
        return mo_coefficients

    @property
    def natural_occupations(self) -> Dict[int, np.ndarray]:
        """Extract natural orbital occupation numbers from Dalton output file."""
        with open(f'{self.filename}.out', 'r') as f:
            lines = [line.strip() for line in f.readlines()]
        # Find number of symmetry species.
        for line in lines:
            if '@    Total number of symmetries' in line:
                num_symmetries = int(line.split()[5])
                break
        else:
            raise Exception(f'Could not find number of symmetry elements in {self.filename}.')
        nat_occ_dict: Dict[int, List[float]] = {}
        for i in range(1, num_symmetries + 1):
            nat_occ_dict[i] = []
        # Find natural occupations.
        current_symmetry = 0
        for line in lines:
            if 'Time used for MP2 natural orbitals' in line:
                break
            elif 'GETNO: the orbital coefficients are transformed to natural orbitals.' in line:
                break
            elif 'No occupied orbitals' in line:
                continue
            elif 'Natural orbital occupation numbers, symmetry' in line:
                # For MP2 output files.
                current_symmetry = int(line.split()[5])
            elif 'Total occupation in this symmetry is' in line:
                # For RASCI output files.
                current_symmetry = int(line.split()[1])
            elif current_symmetry > 0 and 'Sum' not in line:
                for nat_occ in line.split():
                    nat_occ_dict[current_symmetry].append(float(nat_occ))
        for key in nat_occ_dict:
            if nat_occ_dict[key]:
                break
        else:
            raise Exception(f'Could not find natural occupations in {self.filename}.')
        return {key: np.array(value) for key, value in nat_occ_dict.items()}

    @property
    def hyperfine_couplings(self) -> HyperfineCouplings:
        """Extract hyperfine couplings from Dalton output file."""
        # TODO handle symmetry
        pattern = r'TRIPLET.* LAGRANGIAN:\s+(-?[\d.]+) AVERAGE:\s+(-?[\d.]+) TOTAL:\s+(-?[\d.]+)'
        with open(f'{self.filename}.out') as f:
            content = f.read()
            matches = re.findall(pattern, content)
        if not matches:
            raise Exception(f'Hyperfine couplings not found in {self.filename}.')
        hfc_polarization = np.array([float(m[0]) for m in matches]).astype(np.float64)
        hfc_direct = np.array([float(m[1]) for m in matches]).astype(np.float64)
        hfc_total = np.array([float(m[2]) for m in matches]).astype(np.float64)
        return HyperfineCouplings(polarization=hfc_polarization, direct=hfc_direct, total=hfc_total)

    @property
    def gradients(self) -> np.ndarray:
        """Extract molecular gradients from Dalton output file."""
        # TODO handle symmetry
        with open(f'{self.filename}.out', 'r') as f:
            lines = [line.strip() for line in f.readlines()]
        if '@    Point group: C1' not in lines:
            raise Exception('Extraction of molecular gradients from Dalton output file is only supported without '
                            'use of symmetry.')
        for line in lines:
            if 'Total number of atoms:' in line:
                num_atoms = int(line.split()[4])
                break
        else:
            raise Exception(f'Number of atoms not found in {self.filename}.')
        gradients = np.zeros((num_atoms, 3))
        gradient_start = 0
        for i, line in enumerate(lines):
            if 'Molecular gradient (au)' in line:
                gradient_start = i + 3
        if gradient_start == 0:
            raise Exception(f'Molecular gradients not found in {self.filename}.')
        for i, line in enumerate(lines[gradient_start:gradient_start + num_atoms]):
            gradients[i, :] = np.array(line.split()[-3:]).astype(np.float64)
        return gradients

    @property
    def hessian(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def dipole_gradients(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def polarizability_gradients(self) -> PolarizabilityGradients:
        raise NotImplementedError

    @property
    def final_geometry(self) -> np.ndarray:
        """Extract final geometry from Dalton output file (not implemented)."""
        raise NotImplementedError('final geometry is not implemented for Dalton')


def extract_symmetry_species(filename: str) -> List[str]:
    """Extract symmetry species from Dalton output file.

    Args:
        filename: name of Dalton output file.

    Returns
        List of symmetry species.
    """
    with open(f'{filename}.out', 'r') as f:
        lines = f.readlines()
    for i, line in enumerate(lines):
        if '@    Abelian symmetry species' in line:
            species = lines[i + 1].split()[2:]
            break
    else:
        raise Exception(f'Could not find Abelian symmetry species in {filename}.out.')
    return species
