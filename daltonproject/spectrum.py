#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from typing import Callable

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.axes import Axes
from qcelemental import PhysicalConstantsContext

from .program import OutputParser
from .vibrational_analysis import VibrationalAnalysis

constants = PhysicalConstantsContext('CODATA2018')


def plot_spectrum(x: np.ndarray, y: np.ndarray, xlabel: str, ylabel: str, ax: Axes,
                  **plot_kwargs) -> Axes:  # pragma: no cover
    """Plot spectrum."""
    ax.plot(x, y, **plot_kwargs)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xlim(np.min(x), np.max(x))
    ax.set_ylim(0, np.max(y) * 1.05)
    return ax


def lorentzian(x: np.ndarray, x0: float, broadening_factor: float) -> np.ndarray:
    """Normalized Lorentzian broadening function.

    Args:
        x: Where to evaluate the Lorentzian.
        x0: Center of Lorentzian.
        broadening_factor: Half width at half maximum.

    Returns:
        A Lorentzian.
    """
    return broadening_factor / (np.pi * (broadening_factor**2 + (x - x0)**2))


def gaussian(x: np.ndarray, x0: float, broadening_factor: float) -> np.ndarray:
    """Normalized Gaussian broadening function.

    Args:
        x: Where to evaluate the Gaussian.
        x0: Center of Gaussian.
        broadening_factor: Half width at half maximum.

    Returns:
        A Gaussian.
    """
    normalize_factor = np.sqrt((np.log(2) / broadening_factor**2) / np.pi)
    return normalize_factor * np.exp(-np.log(2) * ((x-x0) / broadening_factor)**2)


def convolute_one_photon_absorption(excitation_energies: np.ndarray,
                                    oscillator_strengths: np.ndarray,
                                    energy_range: np.ndarray,
                                    broadening_function: Callable[[np.ndarray, float, float],
                                                                  np.ndarray] = gaussian,
                                    broadening_factor: float = 0.4) -> np.ndarray:
    """Convolute one-photon absorption.

    The convolution is based on the equations of:
    A. Rizzo, S. Coriani, and K. Ruud,
    in Computational Strategies for Spectroscopy.
    From Small Molecules to Nano Systems,
    edited by V. Barone (John Wiley and Sons, 2012) Chap. 2, pp. 77–135

    Args:
        excitation_energies: Excitation energies in eV.
        oscillator_strengths: Oscillator strengths.
        energy_range: Energy range of the spectrum in eV.
        broadening_function: Broadening function.
        broadening_factor: Broadening factor in eV.

    Returns:
        Convoluted spectrum of molar absorptivity in L / (mol * cm).
    """
    result = np.zeros(energy_range.shape)
    broadening_factor_au = broadening_factor / constants.hartree2ev
    energy_range_au = energy_range / constants.hartree2ev
    excitation_energies_au = excitation_energies / constants.hartree2ev
    # 4.361314937e19 is derived from simplifying the constants:
    # e**2 * pi * N_A / ( 2 * ln(10) * epsilon_0 * m_e * c ) into units of L / ( mol * cm * s )
    # 2 * pi * 6.57968e15 is to convert the broadening factor from au to s**-1 (angular frequency).
    # Combining the two numbers above gives 6628.460563431657 / ( 2 * pi ).
    prefactor = 6628.460563431657 / (2 * np.pi)
    for excitation_energy_au, oscillator_strength in zip(excitation_energies_au, oscillator_strengths):
        result += (prefactor * oscillator_strength * energy_range_au / excitation_energy_au
                   * broadening_function(energy_range_au, excitation_energy_au, broadening_factor_au))
    return result


def plot_one_photon_spectrum(output: OutputParser,
                             ax: Axes = None,
                             energy_range: np.ndarray = None,
                             broadening_function: Callable[[np.ndarray, float, float], np.ndarray] = gaussian,
                             broadening_factor: float = 0.4,
                             **plot_kwargs) -> Axes:  # pragma: no cover
    """Plot one-photon absorption spectrum.

    Args:
        output: OutputParser object that contains excitation energies in eV
            and oscillator strengths.
        ax: Axes object (matplotlib). If specified, plot will be added to the
            provided Axes object, otherwise a new one is created.
        energy_range: Energy range of the plot in eV. It will be generated
            from the excitation energies if it is not specified.
        broadening_function: Broadening function.
        broadening_factor: Broadening factor in eV.
        plot_kwargs: Optional keyword arguments to be passed directly to the
            matplotlib plot function (for example color='k').

    Returns:
        Axes object with a plot of energy (eV) vs molar absorptivity
        (L / (mol * cm)).
    """
    if energy_range is None:
        e_min = np.min(output.excitation_energies) - broadening_factor*4
        e_min = max(0, e_min)
        e_max = np.max(output.excitation_energies) + broadening_factor*4
        energy_range = np.arange(e_min, e_max + 1e-3, 1e-3)
    spectrum = convolute_one_photon_absorption(excitation_energies=output.excitation_energies,
                                               oscillator_strengths=output.oscillator_strengths,
                                               energy_range=energy_range,
                                               broadening_function=broadening_function,
                                               broadening_factor=broadening_factor)
    if ax is None:
        ax = plt.axes(label='1PA')
    ax = plot_spectrum(x=energy_range,
                       y=spectrum,
                       xlabel=r'$\Delta$E / (eV)',
                       ylabel=r'$\epsilon$ / (L mol$^{-1}$ cm$^{-1}$)',
                       ax=ax,
                       **plot_kwargs)
    return ax


def convolute_two_photon_absorption(excitation_energies: np.ndarray,
                                    two_photon_strengths: np.ndarray,
                                    energy_range: np.ndarray,
                                    broadening_function: Callable[[np.ndarray, float, float],
                                                                  np.ndarray] = lorentzian,
                                    broadening_factor: float = 0.1) -> np.ndarray:
    """Convolute two-photon absorption.

    Args:
        excitation_energies: Excitation energies in eV.
        two_photon_strengths: Two-photon absorption strengths in au.
        energy_range: Energy range of the spectrum in eV.
        broadening_function: Broadening function.
        broadening_factor: Broadening factor in eV.

    Returns:
        Convoluted spectrum of two-photon absorption cross-section in GM.
    """
    result = np.zeros(energy_range.shape)
    energy_range_au = energy_range / constants.hartree2ev
    excitation_energies_au = excitation_energies / constants.hartree2ev
    broadening_factor_au = broadening_factor / constants.hartree2ev
    au2gm = constants.h / (2 * np.pi) / constants.hartree2J * constants.bohr2cm**4 * 1e50
    prefactor = 8 * (np.pi**3) * au2gm / (constants.c_au**2)
    for excitation_energy_au, two_photon_strength in zip(excitation_energies_au, two_photon_strengths):
        result += (prefactor * two_photon_strength * energy_range_au**2
                   * broadening_function(2 * energy_range_au, excitation_energy_au, broadening_factor_au))
    return result


def plot_two_photon_spectrum(output: OutputParser,
                             ax: Axes = None,
                             energy_range: np.ndarray = None,
                             broadening_function: Callable[[np.ndarray, float, float], np.ndarray] = gaussian,
                             broadening_factor: float = 0.4,
                             **plot_kwargs) -> Axes:  # pragma: no cover
    """Plot two-photon absorption spectrum.

    Args:
        output: OutputParser object that contains excitation energies in eV and
            two-photon absorption strengths in au.
        ax: Axes object (matplotlib). If specified, plot will be added to the
            provided Axes object, otherwise a new one is created.
        energy_range: Energy range of the plot in eV. It will be generated
            from the excitation energies if it is not specified.
        broadening_function: Broadening function.
        broadening_factor: Broadening factor in eV.
        plot_kwargs: Optional keyword arguments to be passed directly to the
            matplotlib plot function (for example color='k').

    Returns:
        Axes object with a plot of energy (eV) vs two-photon absorption
        cross-section (GM).
    """
    if energy_range is None:
        e_min = (np.min(output.excitation_energies) - broadening_factor*4) / 2
        e_min = max(0, e_min)
        e_max = (np.max(output.excitation_energies) + broadening_factor*4) / 2
        energy_range = np.arange(e_min, e_max + 1e-3, 1e-3)
    spectrum = convolute_two_photon_absorption(excitation_energies=output.excitation_energies,
                                               two_photon_strengths=output.two_photon_strengths,
                                               energy_range=energy_range,
                                               broadening_function=broadening_function,
                                               broadening_factor=broadening_factor)
    if ax is None:
        ax = plt.axes(label='2PA')
    ax = plot_spectrum(x=energy_range,
                       y=spectrum,
                       xlabel=r'$\Delta$E / (eV)',
                       ylabel=r'$\sigma^{\mathrm{2PA}}$ / (GM)',
                       ax=ax,
                       **plot_kwargs)
    return ax


def convolute_vibrational_spectrum(vibrational_frequencies: np.ndarray,
                                   intensities: np.ndarray,
                                   energy_range: np.ndarray,
                                   broadening_function: Callable[[np.ndarray, float, float],
                                                                 np.ndarray] = lorentzian,
                                   broadening_factor: float = 3.0) -> np.ndarray:
    """Convolute vibrational spectrum (e.g. IR and Raman).

    Args:
        vibrational_frequencies: Vibrational frequencies in cm^-1.
        intensities: Intensities in SI unit.
        energy_range: Energy range of the spectrum in cm^-1.
        broadening_function: Broadening function.
        broadening_factor: Broadening factor in cm^-1.

    Returns:
        Convoluted spectrum in unit of the intensities * s.
    """
    vibrational_frequencies_hz = vibrational_frequencies * 1.0e2 * constants.c
    energy_range_hz = energy_range * 1.0e2 * constants.c
    broadening_factor_hz = broadening_factor * 1.0e2 * constants.c
    result = np.zeros(energy_range_hz.shape)
    for frequency_hz, intensity in zip(vibrational_frequencies_hz, intensities):
        result += intensity * broadening_function(energy_range_hz, frequency_hz, broadening_factor_hz)
    return result


def plot_ir_spectrum(vibrational_analysis: VibrationalAnalysis,
                     ax: Axes = None,
                     energy_range: np.ndarray = None,
                     broadening_function: Callable[[np.ndarray, float, float], np.ndarray] = lorentzian,
                     broadening_factor: float = 3.0,
                     **plot_kwargs) -> Axes:  # pragma: no cover
    """Plot IR spectrum.

    Args:
        vibrational_analysis: VibrationalAnalysis object that contains
            frequencies in cm^-1 and IR intensities in m^2 / (s * mol).
        ax: Axes object (matplotlib). If specified, plot will be added to the
            provided Axes object, otherwise a new one is created.
        energy_range: Energy range of the plot in cm^-1. It will be generated
            from the vibrational frequencies if it is not specified.
        broadening_function: Broadening function.
        broadening_factor: Broadening factor in cm^-1.
        plot_kwargs: Optional keyword arguments to be passed directly to the
            matplotlib plot function (for example color='k').

    Returns:
        Axes object with a plot of energy (cm^-1) vs molar absorptivity
        (m^2 / mol).
    """
    if vibrational_analysis.ir_intensities is None:
        raise ValueError('No IR intensities available.')
    if energy_range is None:
        f_min = np.min(vibrational_analysis.frequencies) - broadening_factor*4 - 100.0
        f_min = max(0, f_min)
        f_max = np.max(vibrational_analysis.frequencies) + broadening_factor*4 + 100.0
        energy_range = np.arange(f_min, f_max + 0.1, 0.1)
    spectrum = convolute_vibrational_spectrum(vibrational_frequencies=vibrational_analysis.frequencies,
                                              intensities=vibrational_analysis.ir_intensities,
                                              energy_range=energy_range,
                                              broadening_function=broadening_function,
                                              broadening_factor=broadening_factor)
    if ax is None:
        ax = plt.axes(label='IR')
    ax = plot_spectrum(x=energy_range,
                       y=spectrum,
                       xlabel=r'$\tilde\nu$ / (cm$^{-1}$)',
                       ylabel=r'$\epsilon$ / (m$^{2}$ mol$^{-1}$)',
                       ax=ax,
                       **plot_kwargs)
    return ax


def plot_raman_spectrum(vibrational_analysis: VibrationalAnalysis,
                        ax: Axes = None,
                        energy_range: np.ndarray = None,
                        broadening_function: Callable[[np.ndarray, float, float], np.ndarray] = lorentzian,
                        broadening_factor: float = 3.0,
                        **plot_kwargs) -> Axes:  # pragma: no cover
    """Plot Raman spectrum.

    Args:
        vibrational_analysis: VibrationalAnalysis object that contains
            frequencies in cm^-1 and Raman intensities in C^4*s^2/(J*m^2*kg).
        ax: Axes object (matplotlib). If specified, plot will be added to the
            provided Axes object, otherwise a new one is created.
        energy_range: Energy range of the plot in cm^-1. It will be generated
            from the vibrational frequencies if it is not specified.
        broadening_function: Broadening function.
        broadening_factor: Broadening factor in cm^-1.
        plot_kwargs: Optional keyword arguments to be passed directly to the
            matplotlib plot function (for example color='k').

    Returns:
        Axes object with a plot of energy (cm^-1) vs Raman scattering
        cross-section (C^4*s^3/(J*m^2*kg)).
    """
    if vibrational_analysis.raman_intensities is None:
        raise ValueError('No Raman intensities available.')
    if energy_range is None:
        f_min = np.min(vibrational_analysis.frequencies) - broadening_factor*4 - 100.0
        f_min = max(0, f_min)
        f_max = np.max(vibrational_analysis.frequencies) + broadening_factor*4 + 100.0
        energy_range = np.arange(f_min, f_max + 0.1, 0.1)
    for intensities in vibrational_analysis.raman_intensities:
        spectrum = convolute_vibrational_spectrum(vibrational_frequencies=vibrational_analysis.frequencies,
                                                  intensities=intensities,
                                                  energy_range=energy_range,
                                                  broadening_function=broadening_function,
                                                  broadening_factor=broadening_factor)
        if ax is None:
            ax = plt.axes(label='Raman')
        ax = plot_spectrum(x=energy_range,
                           y=spectrum,
                           xlabel=r'$\tilde\nu$ / (cm$^{-1}$)',
                           ylabel=r'$\dfrac{\mathrm{d}\sigma}{\mathrm{d}\Omega}$'
                           + r' / (C$^{4}$ s$^{3}$ J$^{-1}$ m$^{-2}$ kg$^{-1})$',
                           ax=ax,
                           **plot_kwargs)
    return ax
