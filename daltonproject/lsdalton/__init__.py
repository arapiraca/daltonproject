#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from .integrals import (coulomb_matrix, diagonal_density, eri, eri4, exchange_correlation, exchange_matrix,
                        fock_matrix, kinetic_energy_matrix, nuclear_electron_attraction_matrix, num_atoms,
                        num_basis_functions, num_electrons, num_ri_basis_functions, overlap_matrix, ri2, ri3)
from .output_parser import OutputParser
from .program import LSDalton

compute = LSDalton.compute
compute_farm = LSDalton.compute_farm

__all__ = (
    'compute',
    'compute_farm',
    'OutputParser',
    'num_atoms',
    'num_electrons',
    'num_basis_functions',
    'num_ri_basis_functions',
    'overlap_matrix',
    'kinetic_energy_matrix',
    'nuclear_electron_attraction_matrix',
    'coulomb_matrix',
    'exchange_matrix',
    'exchange_correlation',
    'fock_matrix',
    'eri',
    'eri4',
    'ri3',
    'ri2',
    'diagonal_density',
)
