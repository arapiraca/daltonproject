#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from typing import Dict, List, Union

import basis_set_exchange as bse


class Basis:
    """Specify the AO basis."""

    def __init__(self,
                 basis: Union[Dict[str, str], str],
                 ri: Union[Dict[str, str], str] = None,
                 admm: Union[Dict[str, str], str] = None) -> None:
        """Initialize Basis instance.

        Args:
            basis: Basis set for molecule. Can be a string, if same basis set
                should be applied to all atoms. Can be a dict of the type
                {atom_name: basis} if different basis sets are to be used for
                different atoms.
            ri: Auxiliary basis set for RI (optional). Follows same format as basis.
            admm: Auxiliary basis set for ADMM (optional). Follows same format as basis.
        """
        validate_basis(basis)
        self.basis = basis
        if ri is not None:
            validate_basis(ri)
        self.ri = ri
        if admm is not None:
            validate_basis(admm)
        self.admm = admm

    def write(self, basis_format: str = 'dalton') -> None:
        for basis_set in (self.basis, self.ri, self.admm):
            # ri and admm sets can be empty
            if not basis_set:
                continue
            # basis can be either a str or dict
            # so we make a dummy dict for when it is str
            if isinstance(basis_set, str):
                basis_dict = {'dummy': basis_set}
            else:
                basis_dict = basis_set
            for basis in basis_dict.values():
                bse_basis = bse.get_basis(name=basis)
                bse.write_formatted_basis_file(basis_dict=bse_basis, outfile_path=basis, basis_fmt=basis_format)


def validate_basis(basis: Union[Dict[str, str], str]) -> None:
    """Validate basis set input.

    Args:
        basis: Basis set (str or dict)
    """
    if not isinstance(basis, str) and not isinstance(basis, dict):
        raise TypeError(f'Unsupported basis set type: {type(basis)}.')
    elif isinstance(basis, dict):
        for label, basis_type in basis.items():
            if not isinstance(label, str):
                raise TypeError(f'Invalid atom label type: {type(label)}.')
            if not isinstance(basis_type, str):
                raise TypeError(f'Unknown basis type: {type(basis_type)}.')


def get_atom_basis(basis: Union[str, Dict[str, str]], num_atoms: int, labels: List[str]) -> List[str]:
    """Process basis set input.

    Args:
        basis: Basis set.
        num_atoms: Number of atoms.
        labels: Atom labels.

    Returns:
        List containing the basis set of each individual atoms.
    """
    if isinstance(basis, str):
        atom_basis = [basis] * num_atoms
    elif isinstance(basis, dict):
        atom_basis = []
        for label in labels:
            try:
                atom_basis.append(basis[label])
            except KeyError:
                raise KeyError(f'no basis for {label} was provided.')
    else:
        raise TypeError(f'basis must be of type str or dict and not {type(basis)}.')
    return atom_basis
