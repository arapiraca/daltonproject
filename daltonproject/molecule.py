#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

import os
import warnings
from typing import List, Union

import numpy as np
from qcelemental import periodictable

from .symmetry import SymmetryAnalysis


class Molecule:
    """Molecule class."""

    supported_filetypes = ('.xyz', )

    def __init__(self, atoms: str = None, charge: int = 0, input_file: str = None, symmetry: bool = False) -> None:
        """Initialize Molecule instance.

        Args:
            atoms: Atoms specified with elements and coordinates (and optionally labels).
            charge: Total charge of molecule.
            input_file: Input file containing atoms and coordinates (and optionally labels).
            symmetry: Use symmetry detection.
        """
        if not isinstance(charge, int):
            raise TypeError('Charge must be an integer.')
        self.charge: int = charge
        self.elements: List[str] = []
        self.masses: np.ndarray = np.array([])
        self.labels: List[str] = []
        self.symmetry_threshold: float = 1e-3
        self.point_group: str = 'C1'
        self._coordinates: np.ndarray = np.array([])
        if atoms is not None and input_file is not None:
            raise SyntaxError('Specify either atoms or input_file.')
        if atoms is None and input_file is None:
            raise SyntaxError('Specify either atoms or input_file.')
        if input_file is not None:
            if not os.path.isfile(input_file):
                raise FileNotFoundError(f'{input_file} not found.')
            file_name, file_ext = os.path.splitext(input_file)
            # check if file type is supported, i.e. is there a method corresponding to the file extension
            if hasattr(self, file_ext.strip('.')):
                # call method corresponding to file extension, e.g. if input_file has file extension '.xyz' then
                # this calls self.xyz(input_file)
                getattr(self, file_ext.strip('.'))(input_file)
            else:
                raise NotImplementedError(f'File type "{file_ext}" is not supported. Available file types are:\n'
                                          f'{self.supported_filetypes}')
        if atoms is not None:
            self.atoms(atoms)
        self.symmetry: SymmetryAnalysis = SymmetryAnalysis(self.elements,
                                                           self.coordinates,
                                                           symmetry_threshold=self.symmetry_threshold,
                                                           labels=self.labels)
        if symmetry:
            self.analyse_symmetry()

    def atoms(self, atoms: str) -> None:
        """Specify atoms.

        Args:
            atoms: Atoms specified with elements and coordinates (and optionally labels).
        """
        if not isinstance(atoms, str):
            raise TypeError('Atoms are given as a string using either "\\n" or ";" to separate atoms.')
        # get rid of final new line if present
        if atoms[-1:] == '\n':
            atoms = atoms[:-1]
        if ';' in atoms:
            lines = atoms.split(';')
        elif '\n' in atoms:
            lines = atoms.split('\n')
        elif not 4 <= len(atoms.split()) <= 5:
            raise ValueError(f'Incompatible input: {atoms}')
        else:
            # Assume it is a single atom.
            lines = [atoms]
        if lines[-1:] == ['']:
            lines = lines[:-1]
        elements = []
        masses = []
        labels = []
        coordinates = []
        for line in lines:
            atom = line.split()
            element = atom[0].strip().title()
            if element not in periodictable.E:
                raise ValueError(f'Unknown element: {element}.')
            elements.append(element)
            masses.append(periodictable.to_mass(element))
            coordinates.append(np.array([float(value) for value in atom[1:4]]))
            if len(atom) == 5:
                labels.append(atom[4].strip())
            else:
                labels.append(atom[0].strip())
        self.elements = elements
        self.masses = np.array(masses)
        self.labels = labels
        self.coordinates = np.array(coordinates)

    def xyz(self, filename: str) -> None:
        """Read xyz file.

        Args:
            filename: Name of xyz file containing atoms and coordinates.
        """
        with open(f'{filename}', 'r') as xyz_file:
            lines = xyz_file.read().split('\n')
        try:
            num_atoms = int(lines[0])
        except ValueError:
            raise TypeError('First line in xyz file cannot be converted to an integer.')
        # Remove trailing empty lines.
        while lines[-1].strip() == '':
            del lines[-1]
        if len(lines[2:]) > num_atoms:
            warnings.warn(f'Only {num_atoms} atoms were declared but xyz file apparently contains {len(lines[2:])}'
                          f' lines. Only {num_atoms} will be read.')
        if len(lines[2:]) < num_atoms:
            raise ValueError(f'{num_atoms} atoms were declared, but file only contains {len(lines[2:])} lines.')
        atom_lines = ''
        # Skip the declaration and comment line.
        for line in lines[2:num_atoms + 2]:
            coordinate_line = line.split()
            # We allow lines to be longer than 4 and 5 values in case the user adds comments etc.
            if len(coordinate_line) < 4:
                raise ValueError(f'Wrong number of values in line:\n{line}')
            if coordinate_line[0].strip().title() not in periodictable.E:
                raise ValueError(f'Unknown element in line:\n{line}.')
            try:
                [float(value) for value in coordinate_line[1:4]]
            except ValueError:
                raise TypeError(f'Cannot read coordinates from line:\n{line}')
            # Add five values in case there are atom labels but remove the rest (if present).
            atom_lines += ' '.join(coordinate_line[0:5]) + '\n'
        # The slice is to remove the last new-line character.
        self.atoms(atom_lines[:-1])

    @property
    def coordinates(self) -> np.ndarray:
        """Atomic coordinates."""
        return self._coordinates

    @coordinates.setter
    def coordinates(self, coordinates: Union[List[List[float]], np.ndarray]) -> None:
        if not len(coordinates) == self.num_atoms:
            raise Exception('Number of coordinates does not correspond to number of atoms in molecule.')
        if isinstance(coordinates, np.ndarray):
            self._coordinates = coordinates
        elif isinstance(coordinates, list):
            self._coordinates = np.array(coordinates)

    @property
    def num_atoms(self) -> int:
        """Number of atoms in molecule."""
        return len(self.elements)

    def analyse_symmetry(self) -> None:
        """Analyse the molecular symmetry. Note that this translates the molecules center of mass to the origin."""
        self.symmetry = SymmetryAnalysis(self.elements,
                                         self.coordinates,
                                         symmetry_threshold=self.symmetry_threshold,
                                         labels=self.labels)
        self.coordinates, self.point_group = self.symmetry.find_symmetry()
