import daltonproject as dp

mp2_output = dp.dalton.OutputParser('52c2525a4e1901f5a77a81991e7cf92e2aeaf79e')
nat_occs = mp2_output.natural_occupations
print(dp.natural_occupation.scan_occupations(nat_occs))

scan_nat_occs = dp.natural_occupation.scan_occupations(nat_occs)
assert scan_nat_occs == 'Strongly occupied natural orbitals                      Weakly occupied natural orbitals\n' \
                        'Symmetry   Occupation   Change in occ.   Diff. to 2     Symmetry   Occupation   Change in' \
                        ' occ.\n   2          1.9345        0.0000        0.0655            2          0.0698      ' \
                        '  0.0000\n   4          1.9367        0.0022        0.0633            4          0.0630   ' \
                        '     0.0068\n   2          1.9674        0.0307        0.0326            2          0.0325' \
                        '        0.0304\n   3          1.9766        0.0093        0.0234            3          ' \
                        '0.0218        0.0107\n   1          1.9782        0.0016        0.0218            1      ' \
                        '    0.0207        0.0011\n   3          1.9788        0.0006        0.0212            3   ' \
                        '       0.0180        0.0027\n   1          1.9832        0.0044        0.0168            1' \
                        '          0.0175        0.0005\n   3          1.9835        0.0003        0.0165          ' \
                        '  1          0.0166        0.0009\n   1          1.9862        0.0026        0.0138       ' \
                        '     3          0.0128        0.0038\n   1          1.9870        0.0008        0.0130    ' \
                        '        1          0.0126        0.0002\n   3          1.9886        0.0016        0.0114 ' \
                        '           1          0.0119        0.0007\n   1          1.9895        0.0009        ' \
                        '0.0105            3          0.0119        0.0000\n   3          1.9908        0.0013     ' \
                        '   0.0092            3          0.0118        0.0001\n   1          1.9924        0.0015  ' \
                        '      0.0076            1          0.0105        0.0012\n'
