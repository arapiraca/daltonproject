import daltonproject as dp

molecule, basis = dp.mol_reader('pyridine.mol')
molecule.analyse_symmetry()
mp2_output = dp.dalton.OutputParser('52c2525a4e1901f5a77a81991e7cf92e2aeaf79e')

casscf = dp.QCMethod('CASSCF')
prop = dp.Property(energy=True)
nat_occs = mp2_output.natural_occupations
electrons, cas, inactive = dp.natural_occupation.pick_cas_by_thresholds(nat_occs, 1.94, 0.06)
casscf.complete_active_space(electrons, cas, inactive)
casscf.input_orbital_coefficients(mp2_output.orbital_coefficients)
casscf_result = dp.dalton.compute(molecule, basis, casscf, prop)
print('CASSCF energy =', casscf_result.energy)
# CASSCF energy = -243.699847108852

if True:  # To keep test imports from going to the top of the file.
    import numpy as np
assert electrons == 4
assert cas == [0, 2, 0, 2]
assert inactive == [11, 1, 7, 0]
np.testing.assert_allclose(casscf_result.energy, -243.699847108852)
