import matplotlib.pyplot as plt

import daltonproject as dp

acrolein = dp.Molecule(input_file='acrolein.xyz')
basis = dp.Basis('STO-3G')

cc = dp.QCMethod('CC2')
exc = dp.Property(excitation_energies={'states': 6, 'cvseparation': [2, 3, 4]})

settings = dp.ComputeSettings(mpi_num_procs=1)

result = dp.dalton.compute(acrolein, basis, cc, exc, settings)

print('Excitation energies =', result.excitation_energies)
# Excitation energies = [288.23439 288.37014 289.96145 293.82649 294.58115 296.95772]

print('Oscillator strengths  =', result.oscillator_strengths)
# Oscillator strengths  = [0.06045523 0.0547764  0.0606439  0.01915189 0.00328921 0.01109897]

ax = dp.spectrum.plot_one_photon_spectrum(result, color='k')
plt.savefig('cvs_cc.svg')

if True:
    import numpy as np
np.testing.assert_allclose(result.excitation_energies,
                           [288.23439, 288.37014, 289.96145, 293.82649, 294.58115, 296.95772],
                           atol=1e-5)
np.testing.assert_allclose(result.oscillator_strengths,
                           [0.060455, 0.054776, 0.060644, 0.019152, 0.003289, 0.011099],
                           atol=1e-6)
