import daltonproject as dp

hf = dp.QCMethod('HF')
basis = dp.Basis(basis='pcseg-1')
opa = dp.Property(energy=True)
molecules = [
    dp.Molecule(input_file='data/water.xyz'),
    dp.Molecule(input_file='data/ethanol.xyz'),
    dp.Molecule(input_file='data/H2O2.xyz')
]

settings = dp.ComputeSettings(mpi_num_procs=6, jobs_per_node=2)
dalton_results = dp.dalton.compute_farm(molecules, basis, hf, opa, settings)
for dalton_result in dalton_results:
    print(dalton_result.energy)

settings = dp.ComputeSettings(jobs_per_node=2)
lsdalton_results = dp.lsdalton.compute_farm(molecules, basis, hf, opa, settings)
for lsdalton_result in lsdalton_results:
    print(lsdalton_result.energy)
