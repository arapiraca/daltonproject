import numpy as np

import daltonproject as dp

hf = dp.QCMethod('HF')
basis = dp.Basis(basis='pcseg-0')
tpa = dp.Property(two_photon_absorption=True)
water = dp.Molecule(input_file='data/water.xyz', charge=0)
result = dp.dalton.compute(water, basis, hf, tpa)

frequencies = np.linspace(0.0, 10.0, 1000)
spectrum = dp.spectrum.convolute_two_photon_absorption(result.excitation_energies, result.two_photon_strengths,
                                                       frequencies)
print(spectrum)
