import filecmp

import pytest

import daltonproject as dp
from daltonproject.utilities import chdir


def test_basis_str():
    basis = dp.Basis(basis='6-31G*')
    assert isinstance(basis.basis, str)
    assert basis.basis == '6-31G*'


def test_basis_dict():
    basis_dict = {'O': 'aug-pcseg-2', 'H': 'pcseg-2'}
    basis = dp.Basis(basis=basis_dict)
    assert isinstance(basis.basis, dict)
    for key, value in basis.basis.items():
        assert key in basis_dict


def test_basis_ri_admm():
    basis = dp.Basis(basis='6-31G*', ri='def2-universal-JKFIT', admm='admm-1')
    assert isinstance(basis.basis, str)
    assert isinstance(basis.ri, str)
    assert isinstance(basis.admm, str)
    assert basis.basis == '6-31G*'
    assert basis.ri == 'def2-universal-JKFIT'
    assert basis.admm == 'admm-1'


def test_basis_exception():
    with pytest.raises(TypeError, match=f'Unsupported basis set type: {list}'):
        dp.Basis(basis=['pcseg-2', 'pcseg-1'])
    with pytest.raises(TypeError, match=f'Unknown basis type: {list}'):
        dp.Basis(basis={'X': []})
    with pytest.raises(TypeError, match=f'Invalid atom label type: {int}'):
        dp.Basis(basis={1: 'pcseg-1'})


def test_get_atom_basis():
    atom_basis = dp.basis.get_atom_basis(basis='STO-3G', num_atoms=3, labels=['O1', 'H1', 'H2'])
    assert atom_basis == ['STO-3G', 'STO-3G', 'STO-3G']
    atom_basis = dp.basis.get_atom_basis(
        basis={'H1': 'STO-3G', 'H2': 'STO-3G', 'O1': '6-31G*'},
        num_atoms=3,
        labels=['O1', 'H1', 'H2'],
    )
    assert atom_basis == ['6-31G*', 'STO-3G', 'STO-3G']


def test_get_atom_basis_exceptions():
    with pytest.raises(KeyError, match='no basis for O1 was provided.'):
        dp.basis.get_atom_basis(basis={'H1': 'STO-3G', 'H2': 'STO-3G'}, num_atoms=3, labels=['O1', 'H1', 'H2'])
    with pytest.raises(TypeError, match='basis must be of type str or dict and not'):
        dp.basis.get_atom_basis(basis=['STO-3G'], num_atoms=3, labels=['O1', 'H1', 'H2'])


@pytest.mark.datafiles(
    pytest.DATADIR / '5-21G.ref', )
def test_basis_write(datafiles):
    with chdir(datafiles):
        basis = dp.Basis(basis='5-21G')
        basis.write()
        assert filecmp.cmp('5-21G', datafiles / '5-21G.ref')
