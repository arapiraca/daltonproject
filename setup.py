#!/usr/bin/env python

import os.path as path

from setuptools import find_packages, setup

import daltonproject as dp

with open(path.join(path.abspath(path.dirname(__file__)), 'README.md'), encoding='utf-8') as readme_file:
    long_description = readme_file.read()
description = 'Dalton Project: A Python platform for molecular- and' \
              ' electronic-structure simulations of complex systems'

setup(
    name='daltonproject',
    version=dp.__version__,
    description=description,
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://www.daltonproject.org/',
    project_urls={
        'Documentation': 'https://daltonproject.readthedocs.io/',
        'Source': 'https://gitlab.com/daltonproject/daltonproject/',
        'Issue Tracker': 'https://gitlab.com/daltonproject/daltonproject/issues',
    },
    author=dp.__author__,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Scientific/Engineering :: Chemistry',
        'Topic :: Scientific/Engineering :: Physics',
    ],
    install_requires=[
        'basis_set_exchange==0.8.*',
        'matplotlib>=3,<4',
        'numpy>=1,<2',
        'parse>=1,<2',
        'psutil>=5,<6',
        'qcelemental==0.14.*',
        'spectroscpy==0.1.0',
    ],
    python_requires='>=3.6',
    packages=find_packages(include=['daltonproject', 'daltonproject.*']),
)
